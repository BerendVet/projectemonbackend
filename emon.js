const express = require('express');
const router = express.Router();
const mqtt = require('mqtt');
const settings = require('./settings.json');
const EmonDataMessage = require('./emondatamessage');

// diederich MQTT
let options = {
    host: settings.Dbroker,
    clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username: settings.Dusername,
    password: settings.Dpassword,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQTT',
    protocolVersion: 4,
    clean: true,
    encoding: 'utf8'
};

// our mqtt
// let options = {
//     host: settings.broker,
//     clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
//     keepalive: 60,
//     reconnectPeriod: 1000,
//     protocolId: 'MQTT',
//     protocolVersion: 4,
//     clean: true,
//     encoding: 'utf8'
// };
let client = mqtt.connect(settings.Dbroker, options);

console.log('Connecting to MQTT');

client.on('connect', () => {
    console.log('Connected to MQTT');
    client.subscribe(settings.Dtopic);
});
client.on('error', function(){
    console.log("ERROR")
    client.end()
})

client.on('message', function (topic, message) {
    //obj = JSON.parse(message.toString());
    //parseData(obj.datagram.p1)
    parseData(message.toString());
});

function parseData(p1) {
    let message = {};
    let log = "";
    log = log.concat("\r\n ------New Message ------\r\n");
    let lines = p1.split("\r\n");
    lines.forEach(element => {
        if(element === "") return;
        if(element.startsWith("1-3:0.2.8")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nversion: "+result[1]);
        }
        if(element.startsWith("0-0:1.0.0")){
            let result = element.match(/\((.*)\)/);
            let date = new Date(("20" + result[1].substring(0, result[1].length - 1)).replace(
                /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
                '$4:$5:$6 $2/$3/$1'
            ));
            message.date = date;
            log = log.concat("\r\ntimestamp: "+ date);
        }
        if(element.startsWith("0-0:96.1.1")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nSerienummer: "+ parseInt(result[1]));
        }
        if(element.startsWith("1-0:1.8.1")){
            let result = element.match(/\((.*)\)/);
            message.elec_in_tarif_1 = parseFloat(result[1]);
            log = log.concat("\r\nElektriciteit in Tarief 1:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:1.8.2")){
            let result = element.match(/\((.*)\)/);
            message.elec_in_tarif_2 = parseFloat(result[1]);
            log = log.concat("\r\nElektriciteit in Tarief 2:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:2.8.1")){
            let result = element.match(/\((.*)\)/);
            message.elec_out_tarif_1 = parseFloat(result[1]);
            log = log.concat("\r\nElektriciteit uit Tarief 1:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:2.8.2")){
            let result = element.match(/\((.*)\)/);
            message.elec_out_tarif_2 = parseFloat(result[1]);
            log = log.concat("\r\nElektriciteit uit Tarief 2:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-0:96.14.0")){
            let result = element.match(/\((.*)\)/);
            message.tarif = parseFloat(result[1]);
            log = log.concat("\r\ntarif:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:1.7.0")){
            let result = element.match(/\((.*)\)/);
            message.act_power_cons = parseFloat(result[1]);
            log = log.concat("\r\nActual power consumed:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:2.7.0")){
            let result = element.match(/\((.*)\)/);
            message.act_power_prod = parseFloat(result[1]);
            log = log.concat("\r\nActual power produced:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-0:96.7.21")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nPower failures:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-0:96.7.9")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nLong power failures:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:99.97.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nPower Failure Log:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:32.32.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nVoltage sags L1:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:32.36.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nVoltage swells L1:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-0:96.13.1")){
            let result = element.match(/\((.*)\)/);
            // console.log("tarrif:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-0:96.13.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nText messages max:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:31.7.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nInstantaneous cur- rent L1 in A resolution.:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:21.7.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nInstantaneous active power L1: "+ parseFloat(result[1]));
        } 
        if(element.startsWith("1-0:22.7.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\nInstantaneous active power L2:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-1:24.1.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\n?:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-1:96.1.0")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\n?:"+ parseFloat(result[1]));
        } 
        if(element.startsWith("0-1:24.2.1")){
            let result = element.match(/\((.*)\)/);
            log = log.concat("\r\n?:"+ parseFloat(result[1]));
        }
        if(element.startsWith("!")){
            saveData(message);
            console.log(log);
        }
    });
}

function saveData(obj) {
    const message = new EmonDataMessage(obj);
    message.save().then((data) => {

    }).catch((error) => {
        console.warn(error);//'failed to store');
    })
}

function splitArray(a, n) {

    let out = []
    while(a.length) {
        out.push(a.splice(0,n));
    }
    
    return out;
}

function filterData(data) {
    let filteredData = [];

    let act_power_cons;
    let act_power_prod;
    for (var i = 0; i < data.length; i++) {
        act_power_cons = 0;
        act_power_prod = 0;
        for (var j = 0; j < data[i].length; j++) {
            act_power_cons += data[i][j].act_power_cons;
            act_power_prod += data[i][j].act_power_prod;
            
        }
        let item = {};
        item.act_power_cons = act_power_cons / data[i].length;
        item.act_power_prod = act_power_prod / data[i].length;
        item.tarif = data[i][data[i].length -1].tarif;
        item.elec_in_tarif_1 = data[i][data[i].length-1].elec_in_tarif_1;
        item.elec_in_tarif_2 = data[i][data[i].length-1].elec_in_tarif_2;
        item.elec_out_tarif_1 = data[i][data[i].length-1].elec_out_tarif_1;
        item.elec_out_tarif_2 = data[i][data[i].length-1].elec_out_tarif_2;
        item.date = data[i][data[i].length-1].date;

        filteredData.push(item);
    }
    return filteredData
}

router.get('/getLastHour', (req, res) => {
    let now = Math.round(((new Date()).getTime()) / 1000);
    console.log(now)
    let query = getQuery(new Date(now - (60 * 60)), new Date(now));
    console.log(query);

    let spacing = req.query.spacing || 6;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        res.status(200).json(filterData(splitArray(result, spacing)));
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
});

router.get('/getLastDay', (req, res) => {
    let now = Math.round(((new Date()).getTime()) / 1000);
    console.log(now)
    let query = getQuery(new Date(now - (60 * 60 * 24)), new Date(now));
    console.log(query);

    let spacing = req.query.spacing || 6 * 60;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        res.status(200).json(filterData(splitArray(result, spacing)));
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
});

router.get('/getLastWeek', (req, res) => {
    let now = Math.round(((new Date()).getTime()) / 1000);
    console.log(now)
    let query = getQuery(new Date(now - (60 * 60 * 24 * 7)), new Date(now));
    console.log(query);
    
    let spacing = req.query.spacing || 6 * 60;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        res.status(200).json(filterData(splitArray(result, spacing)));
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
});

router.get('/getLastMonth', (req, res) => {
    let now = Math.round(((new Date()).getTime()) / 1000);
    console.log(now)
    let query = getQuery(new Date(now - (60 * 60 * 24 * 30)), new Date(now));
    console.log(query);
    
    let spacing = req.query.spacing || 6 * 60 * 24;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        res.status(200).json(filterData(splitArray(result, spacing)));
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
});

router.get('/getLastYear', (req, res) => {
    let now = Math.round(((new Date()).getTime()) / 1000);
    console.log(now)
    let query = getQuery(new Date(now - (60 * 60 * 24 * 365)), new Date(now));
    console.log(query);

    let spacing = req.query.spacing || 6 * 60 * 24;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        if(spacing != 1)
            res.status(200).json(filterData(splitArray(result, spacing)));
        else 
            res.status(200).json(result);
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
});

router.get('/getBetween', (req, res) => {

    let query = getQuery(req.query.fromDate, req.query.toDate);
    console.log(query);

    let spacing = req.query.spacing || 1;

    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        if(spacing != 1)
            res.status(200).json(filterData(splitArray(result, spacing)));
        else 
            res.status(200).json(result);
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
})

router.get('/getAll', (req, res) => {

    let query = null;

    let spacing = req.query.spacing || 1;
    
    EmonDataMessage.find(query, {'_id': 0, 'date': 1, 'elec_in_tarif_1': 1, 'elec_in_tarif_2': 1,'elec_out_tarif_1': 1, 'elec_out_tarif_2': 1, 'tarif': 1, 'act_power_cons': 1,'act_power_prod': 1})
    .then(result => {
        res.status(200).json(filterData(splitArray(result, spacing, true)));
    }).catch(error => {
        console.warn('Failed to fetch: ', error);

    })
})

function getQuery(from, to) {

    let query;
    console.log(from);
    console.log(to);
    if(from == null || to == null) {
        query = null;
    } else {
        query = {
            'date': {
            '$gte': new Date(parseInt((from * 1000))).toISOString(),
            '$lte': new Date(parseInt((to * 1000))).toISOString()
            }
        }
    }

    return query;
}



module.exports = router;