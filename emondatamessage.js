const mongoose = require('mongoose');
const schema  = mongoose.Schema;

const EmonDataMessageSchema = new schema(
    {
        elec_in_tarif_1: {
            type: Number,
            required: true
        },        
        elec_in_tarif_2: {
            type: Number,
            required: true
        },        
        elec_out_tarif_1: {
            type: Number,
            required: true
        },        
        elec_out_tarif_2: {
            type: Number,
            required: true
        },        
        tarif: {
            type: Number,
            required: true
        },        
        act_power_cons: {
            type: Number,
            required: true
        },
        act_power_prod: {
            type: Number,
            required: true
        },
        date: {
            type: Date,
            default: Date.now 
        }
    }, {
        timestamps: true,
        toJSON: {
            transform: (doc, ret) => {
                delete ret._id;
                delete ret.__v;
            }
        }
    }
)

const EmonDataMessage = mongoose.model('Emon', EmonDataMessageSchema);
module.exports = EmonDataMessage;