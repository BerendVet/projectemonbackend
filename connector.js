var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

connector = function(app) {
    var settings = app.get('settings');
    var host = settings.dbhost;

    var mongo_url = 'mongodb://' + host + '/cgb_electronics';
    return mongoose.connect(mongo_url, { useNewUrlParser: true })
        .then((db) => {
            console.log('Connected to MongoDB');
        }).catch(error => {
            console.log('Cant connect to MongoDB' + error.toString());
            throw error;
        });
}

module.exports = connector;
