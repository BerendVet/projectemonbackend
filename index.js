var express = require('express');
var parser = require('body-parser');
var settings = require('./settings.json');
var connector = require('./connector.js');
var emon = require('./emon');

console.log('starting');

var app = express();

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(parser.json());
app.use(parser.urlencoded({extended: true}));

app.set('settings', settings);

connector(app);
app.all('*', (req, res, next) => {
    console.log(req.method + ' ' + req.url);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use('/api', emon)

app.listen(3000, function() {
    console.log('Server started');
});
